extern crate clap;
extern crate libc;
extern crate ncurses;
extern crate regex;

mod buffer;
mod errors;
mod lines;
mod pager;

use std::char;
use std::ffi::CString;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::io::stdin;

use libc::{fopen};

use errors::FilterlessError;
use pager::Pager;
use lines::{
    FilterPredicate,
    OptionalFilterPredicate,
};


const LOWER_J: i32 = 0x6a;
const LOWER_K: i32 = 0x6b;
const LOWER_Q: i32 = 0x71;
const FWD_SLASH: i32 = 0x2f;
const CTRL_D: i32 = 4;
const CTRL_U: i32 = 21;
const ENTER: i32 = 10;
const BACKSPACE: i32 = 127;

const MARGIN: i32 = 0;


/// Returns a C-style string from a `&str`.
fn get_cstring(string: &str) -> CString {
    CString::new(string).unwrap()
}

/// Creates and returns an ncurses window which reads user input from tty
/// (to avoid taking commands from a piped file) and writes to stdout.
fn setup_term() -> ncurses::SCREEN {
    let tty;
    let stdout;

    unsafe {
        tty = fopen(get_cstring("/dev/tty").as_ptr(),
                        get_cstring("r").as_ptr());
        stdout = fopen(get_cstring("/dev/stdout").as_ptr(),
                           get_cstring("w").as_ptr());
    }

    let term = ncurses::newterm(None, stdout, tty);
    ncurses::set_term(term);
    ncurses::noecho();
    term
}

/// Presents a CLI and returns a boxed `std::io::BufRead` which enables
/// line-wise reading of input from a file via the CLI or failing that from
/// stdin.
///
/// ### Parameters
/// * `_stdin`: standard input from which to read if user doesn't provide a file
///   name
fn get_input<'a>(
    _stdin: &'a std::io::Stdin,
) -> Result<Box<dyn Iterator<Item = std::io::Result<String>> + 'a>, FilterlessError> {
      let matches = clap::App::new("Filterless")
          .version(env!("CARGO_PKG_VERSION"))
          .author("Michael Wilson")
          .about("Less, but with filtering")
          .arg(
              clap::Arg::with_name("INPUT")
                  .help("paths to input file(s)")
                  .multiple(true)
                  .required(false)
                  .index(1)
          )
          .get_matches();

      match matches.values_of("INPUT") {
          Some(fnames) => {
              fnames
                  .map(File::open)
                  .collect::<Result<Vec<File>, std::io::Error>>()
                  .map(|files| {
                      files
                          .into_iter()
                          .flat_map(|file| BufReader::new(file).lines())
                  })
              .map_err(FilterlessError::from)
              .map(|iter| Box::new(iter) as Box<dyn Iterator<Item = std::io::Result<String>> + 'a>)

          },
          None => {
              let lines = _stdin
                  .lock()
                  .lines();
              Result::Ok(Box::new(lines))
          },
      }
}

/// Event handler for when a user chooses to begin filtering text.
///
/// Spawns a single-line window at the bottom of the screen, collects user
/// input, and returns it after user presses ENTER.
///
/// ### Parameters
/// * `width`: width of the terminal in columns
/// * `height`: height of the terminal in rows
fn _filter<T: Iterator<Item=String>>(height: i32, pager: &mut Pager<T>) {
    let filter_win = ncurses::newwin(1, 0, height - 1, 0);
    ncurses::waddstr(filter_win, "Filter: ");
    ncurses::wrefresh(filter_win);
    let mut filter_str = String::new();
    loop {
        match ncurses::getch() {
            ENTER => break,
            BACKSPACE => {
                if filter_str.pop().is_some() {
                    let mut x = 0;
                    let mut y = 0;
                    ncurses::getyx(filter_win, &mut y, &mut x);
                    ncurses::wmove(filter_win, y, x - 1);
                    ncurses::wdelch(filter_win);
                    ncurses::wrefresh(filter_win);
                }
            },
            ch => {
                filter_str.push(char::from_u32(ch as u32).unwrap());
                ncurses::waddch(filter_win, ch as ncurses::chtype);
                ncurses::wrefresh(filter_win);
            },
        }

        let predicate = if filter_str.is_empty() {
            None
        } else {
            Some(&filter_str)
        }.and_then(|filter_str| regex::Regex::new(filter_str).ok())
        .map(|regex| FilterPredicate {pattern: regex, context_lines: 3});

        pager.filter(OptionalFilterPredicate::new(predicate));
        ncurses::wrefresh(filter_win);
    }

    if filter_str.is_empty() {
        ncurses::wclear(filter_win);
        ncurses::wrefresh(filter_win);
    }

    ncurses::delwin(filter_win);
}

/// System entry point
fn main() -> Result<(), FilterlessError> {
    let _stdin = stdin();
    let lines = get_input(&_stdin)?;

    let window: ncurses::SCREEN = setup_term();

    let mut max_x = 0;
    let mut max_y = 0;
    ncurses::getmaxyx(ncurses::stdscr(), &mut max_y, &mut max_x);
    let height = max_y - MARGIN;
    let width = max_x - MARGIN;

    ncurses::refresh();

    let win = ncurses::newwin(height - 1, width, MARGIN / 2, MARGIN / 2);
    let iter = lines.map(|l| l.unwrap_or_else(|_| String::from("UNICODE ERROR")));
    let mut pager = Pager::new(win, iter);
    pager.next_page();

    loop {
        match ncurses::getch() {
            LOWER_J => pager.next_line(),
            LOWER_K => pager.prev_line(),
            ncurses::KEY_NPAGE | CTRL_D => pager.next_page(),
            ncurses::KEY_PPAGE | CTRL_U => pager.prev_page(),
            FWD_SLASH => {
                _filter(height, &mut pager);
                ncurses::wrefresh(win);
            },
            LOWER_Q => break,
            _ => continue,
        }
    }

    ncurses::endwin();
    ncurses::delscreen(window);

    Ok(())
}

