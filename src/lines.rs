use std::fmt;

use regex::Regex;

#[derive(Clone)]
pub enum IterDirection {
    BACKWARD,
    FORWARD,
}

/// Parameters used when creating a filtering iterator
#[derive(Clone)]
pub struct OptionalFilterPredicate {
    pub maybe_predicate: Option<FilterPredicate>,
}

impl From<&OptionalFilterPredicate> for Option<FilterPredicate> {
    fn from(other: &OptionalFilterPredicate) -> Self {
        other.maybe_predicate.clone()
    }
}

impl OptionalFilterPredicate {
    pub fn new(maybe_predicate: Option<FilterPredicate>) -> Self {
        Self { maybe_predicate }
    }

    pub fn match_line(&self, line: NumberedLine) -> FilteredLine {
        match &self.maybe_predicate {
            Some(FilterPredicate{pattern, ..}) => {
                if pattern.is_match(&line.1) {
                    FilteredLine::MatchLine(line)
                } else {
                    FilteredLine::ContextLine(line)
                }
            },
            None => FilteredLine::UnfilteredLine(line),
        }
    }

    pub fn matches_line(&self, line: &str) -> bool {
        match &self.maybe_predicate {
            Some(FilterPredicate{pattern, ..}) => pattern.is_match(line),
            None => true,
        }
    }

    pub fn get_fragments<'a>(&self, line: &'a str) -> Vec<LineFragment<'a>> {
        match self.maybe_predicate {
            Some(FilterPredicate{ref pattern, ..}) => get_fragments(line, pattern),
            None => vec![LineFragment::NoMatch(line)],
        }
    }
}

#[derive(Clone)]
pub struct FilterPredicate {
    /// regular expression which must match a line to be considered a match
    pub pattern: Regex,
    /// Number of non-match lines above and below a match line to include in
    /// the lines returned by the iterator
    pub context_lines: usize,
}

pub type NumberedLine = (usize, String);

/// Representation of a line that might be returned from a filtering iterator.
#[derive(Clone, Debug, PartialEq)]
pub enum FilteredLine {
    /// a gap between context groups (i.e., groups of context lines
    /// corresponding to distinct match lines)
    Gap,
    /// a line which provides context before or after a matched line
    ContextLine(NumberedLine),
    /// a line matched by a filter string
    MatchLine(NumberedLine),
    /// a line emitted when no filter predicate is in use
    UnfilteredLine(NumberedLine),
}

impl fmt::Display for FilteredLine {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            FilteredLine::Gap => {
                write!(f, "-----")
            },
            FilteredLine::ContextLine((line_num, ref line)) => {
                write!(f, "C {:05}: {}", line_num, line)
            },
            FilteredLine::MatchLine((line_num, ref line)) => {
                write!(f, "M {:05}: {}", line_num, line)
            },
            FilteredLine::UnfilteredLine((line_num, ref line)) => {
                write!(f, "U {:05}: {}", line_num, line)
            },
        }
    }
}

impl From<&FilteredLine> for Option<NumberedLine> {
    fn from(filtered: &FilteredLine) -> Self {
        match filtered {
            FilteredLine::Gap => None,
            FilteredLine::ContextLine(numbered_line) => Some(numbered_line.to_owned()),
            FilteredLine::MatchLine(numbered_line) => Some(numbered_line.to_owned()),
            FilteredLine::UnfilteredLine(numbered_line) => Some(numbered_line.to_owned()),
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum LineFragment<'a> {
    Match(&'a str),
    NoMatch(&'a str),
}

fn get_fragments<'a>(string: &'a str, search: &Regex) -> Vec<LineFragment<'a>> {
    let match_rages = search
        .find_iter(string)
        .map(|_match| (_match.start(), _match.end()))
        .collect::<Vec<_>>();

    if match_rages.is_empty() {
        return vec![LineFragment::NoMatch(string)];
    }

    let mut frags = Vec::new();

    // initial non-match, if it exists
    if match_rages[0].0 > 0 {
        let start = match_rages[0].0;
        frags.push(LineFragment::NoMatch(&string[0..start]));
    }

    for match_idx in 0..match_rages.len() {
        let (start, end) = match_rages[match_idx];
        let match_str = &string[start..end];
        frags.push(LineFragment::Match(match_str));

        let next_match_idx = match_idx + 1;
        if next_match_idx < match_rages.len() {
            let (next_start, _) = match_rages[next_match_idx];
            if end < next_start {
                let match_str = &string[end..next_start];
                frags.push(LineFragment::NoMatch(match_str));
            }
        }
    }

    // final non-match, if it exists
    if match_rages[match_rages.len() - 1].1 < string.len() {
        let end = match_rages[match_rages.len() - 1].1;
        frags.push(LineFragment::NoMatch(&string[end..string.len()]));
    }

    frags
}


#[cfg(test)]
mod test {
    use regex::Regex;
    use super::LineFragment;

    #[test]
    fn get_fragments_basic_test() {
        let string = "the rain in spain";
        let search = Regex::new("rain").unwrap();

        let expected = vec![
            LineFragment::NoMatch("the "),
            LineFragment::Match("rain"),
            LineFragment::NoMatch(" in spain"),
        ];
        let actual = super::get_fragments(string, &search);
        assert_eq!(expected, actual);
    }

    #[test]
    fn get_fragments_no_match_test() {
        let string = "the Rain in spain";
        let search = Regex::new("rain").unwrap();

        let expected = vec![
            LineFragment::NoMatch("the Rain in spain"),
        ];
        let actual = super::get_fragments(string, &search);
        assert_eq!(expected, actual);
    }

    #[test]
    fn get_fragments_start_match_test() {
        let string = "the Rain in spain";
        let search = Regex::new("the").unwrap();

        let expected = vec![
            LineFragment::Match("the"),
            LineFragment::NoMatch(" Rain in spain"),
        ];
        let actual = super::get_fragments(string, &search);
        assert_eq!(expected, actual);
    }

    #[test]
    fn get_fragments_end_match_test() {
        let string = "the Rain in spain";
        let search = Regex::new("spain").unwrap();

        let expected = vec![
            LineFragment::NoMatch("the Rain in "),
            LineFragment::Match("spain"),
        ];
        let actual = super::get_fragments(string, &search);
        assert_eq!(expected, actual);
    }

    #[test]
    fn get_fragments_multi_match_test() {
        let string = "the Rain in spain";
        let search = Regex::new(" ").unwrap();

        let expected = vec![
            LineFragment::NoMatch("the"),
            LineFragment::Match(" "),
            LineFragment::NoMatch("Rain"),
            LineFragment::Match(" "),
            LineFragment::NoMatch("in"),
            LineFragment::Match(" "),
            LineFragment::NoMatch("spain"),
        ];
        let actual = super::get_fragments(string, &search);
        assert_eq!(expected, actual);
    }
} /* test */
