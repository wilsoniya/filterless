use std::fmt;

pub enum FilterlessError {
    // The path to a nonexistent file supplied via CLI
    FileNotFound(String),
}

impl fmt::Debug for FilterlessError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::FileNotFound(fname) => write!(f, "File not found: {}", fname)
        }
    }
}

impl std::convert::From<std::io::Error> for FilterlessError {
    fn from(other: std::io::Error) -> Self {
        FilterlessError::FileNotFound(other.to_string())
    }
}
