use crate::lines::{
    FilteredLine,
    IterDirection,
    OptionalFilterPredicate,
};

use super::{ContextBuffer, LineBuffer};

pub struct WindowBuffer<I: Iterator<Item = String>> {
    last_line_range: (usize, usize),
    page_size: usize,
    buffer: ContextBuffer<I>,
}

impl<I: Iterator<Item = String>> WindowBuffer<I> {
    pub fn new(iter: I, page_size: usize) -> Self {
        let line_buffer = LineBuffer::new(iter);
        let buffer = ContextBuffer::new(line_buffer);

        Self {
            last_line_range: (0, 0),
            page_size,
            buffer,
        }
    }

    pub fn next_line(&mut self) -> Option<FilteredLine> {
        let this_line_num = self.last_line_range.1 + 1;
        let line = self.buffer.get(this_line_num);
        if line.is_some() {
            self.shift_page_range(1);
        }

        line
    }

    pub fn prev_line(&mut self) -> Option<FilteredLine> {
        if self.last_line_range.0 > 1 {
            Some(self.last_line_range.0 - 1)
        } else {
            None
        }.and_then(|this_line_num| {
            let line = self.buffer.get(this_line_num);
            if line.is_some() {
                self.shift_page_range(-1);
            }

            line
        })
    }

    pub fn next_page(&mut self) -> Vec<FilteredLine> {
        let this_line_num = self.last_line_range.1 + 1;
        let page: Vec<_> = self.buffer.iter(this_line_num, IterDirection::FORWARD)
            .take(self.page_size)
            .collect();

        self.shift_page_range(page.len() as i32);

        page
    }

    pub fn prev_page(&mut self) -> Vec<FilteredLine> {
        let this_line_num = if self.last_line_range.0 > 0 { self.last_line_range.0 - 1 } else { 0 };
        let offset = std::cmp::max(0, this_line_num as i64 - self.page_size as i64);
        let num_lines = this_line_num as i64 - offset;

        let page = self.buffer
            .iter(offset as usize, IterDirection::FORWARD)
            .take(num_lines as usize)
            .collect::<Vec<_>>();

        self.shift_page_range(-(page.len() as i32));

        page
    }

    pub fn set_predicate(&mut self, predicate: OptionalFilterPredicate) {
        self.last_line_range = (0, 0);
        self.buffer.set_predicate(predicate);
    }

    fn shift_page_range(&mut self, num_lines: i32) {
        let (prev_start, prev_end) = self.last_line_range;
        let prev_start = prev_start as i32;
        let prev_end = prev_end as i32;
        let page_size = self.page_size as i32;
        let new_start: i32;
        let new_end: i32;

        if num_lines == 0 {
            return
        }

        if prev_start + num_lines < 1 {
            new_start = 1;
            new_end = prev_end - (prev_start - 1);
        } else {
            new_end = prev_end + num_lines;
            new_start = std::cmp::max(new_end - (page_size - 1), 1);
        }

        self.last_line_range = (new_start as usize, new_end as usize);
    }
}

#[cfg(test)]
mod test {
    use crate::lines::FilterPredicate;
    use super::FilteredLine;
    use super::WindowBuffer;
    use super::OptionalFilterPredicate;
    use super::super::LineBuffer;

    #[test]
    fn shift_page_range_test() {
        let lines = "one two three four twosome six seven eight twofer ten eleven"
            .split_whitespace()
            .map(|line| line.to_owned());
        let line_buffer = LineBuffer::new(lines);
        let mut cbv2 = super::ContextBuffer::new(line_buffer);
        cbv2.set_predicate(OptionalFilterPredicate::new(Some(FilterPredicate {
            pattern: regex::Regex::new("two").unwrap(),
            context_lines: 1,
        })));

        let mut obj_ut = WindowBuffer {
            last_line_range: (0, 0),
            page_size: 3,
            buffer: cbv2,
        };

        assert_eq!((0, 0), obj_ut.last_line_range);

        obj_ut.shift_page_range(0);
        assert_eq!((0, 0), obj_ut.last_line_range);

        //// TODO not sure about this one
        //obj_ut.shift_page_range(-1);
        //assert_eq!((1, 1), obj_ut.last_line_range);

        obj_ut.shift_page_range(1);
        assert_eq!((1, 1), obj_ut.last_line_range);

        obj_ut.shift_page_range(1);
        assert_eq!((1, 2), obj_ut.last_line_range);

        obj_ut.shift_page_range(-10);
        assert_eq!((1, 2), obj_ut.last_line_range);

        obj_ut.shift_page_range(1);
        assert_eq!((1, 3), obj_ut.last_line_range);

        obj_ut.shift_page_range(1);
        assert_eq!((2, 4), obj_ut.last_line_range);

        obj_ut.shift_page_range(-1);
        assert_eq!((1, 3), obj_ut.last_line_range);

        obj_ut.shift_page_range(-1);
        assert_eq!((1, 3), obj_ut.last_line_range);

        obj_ut.shift_page_range(2);
        assert_eq!((3, 5), obj_ut.last_line_range);

        obj_ut.shift_page_range(0);
        assert_eq!((3, 5), obj_ut.last_line_range);

        obj_ut.shift_page_range(-10);
        assert_eq!((1, 3), obj_ut.last_line_range);
    }

    #[test]
    fn ops_test() {
        let lines = "one two three four twosome six seven eight twofer ten eleven"
            .split_whitespace()
            .map(|line| line.to_owned());
        let line_buffer = LineBuffer::new(lines);
        let mut cbv2 = super::ContextBuffer::new(line_buffer);
        cbv2.set_predicate(OptionalFilterPredicate::new(Some(FilterPredicate {
            pattern: regex::Regex::new("two").unwrap(),
            context_lines: 1,
        })));

        let mut obj_ut = WindowBuffer {
            last_line_range: (0, 0),
            page_size: 2,
            buffer: cbv2,
        };

        assert_eq!(Some(FilteredLine::ContextLine((1, "one".to_owned()))), obj_ut.next_line());
        assert_eq!((1, 1), obj_ut.last_line_range);
        assert_eq!(Some(FilteredLine::MatchLine((2, "two".to_owned()))), obj_ut.next_line());
        assert_eq!((1, 2), obj_ut.last_line_range);
        assert_eq!(
            vec![
                FilteredLine::ContextLine((3, "three".to_owned())),
                FilteredLine::ContextLine((4, "four".to_owned())),
            ],
            obj_ut.next_page(),
        );
        assert_eq!((3, 4), obj_ut.last_line_range);

        assert_eq!(
            vec![
                FilteredLine::ContextLine((1, "one".to_owned())),
                FilteredLine::MatchLine((2, "two".to_owned())),
            ],
            obj_ut.prev_page(),
        );
        assert_eq!((1, 2), obj_ut.last_line_range);

        assert_eq!(
            vec![] as Vec<FilteredLine>,
            obj_ut.prev_page(),
        );
        assert_eq!((1, 2), obj_ut.last_line_range);

        assert_eq!(
            vec![
                FilteredLine::ContextLine((3, "three".to_owned())),
                FilteredLine::ContextLine((4, "four".to_owned())),
            ],
            obj_ut.next_page(),
        );

        assert_eq!(Some(FilteredLine::MatchLine((5, "twosome".to_owned()))), obj_ut.next_line());
        assert_eq!((4, 5), obj_ut.last_line_range);
        assert_eq!(Some(FilteredLine::ContextLine((3, "three".to_owned()))), obj_ut.prev_line());
        assert_eq!(Some(FilteredLine::MatchLine((2, "two".to_owned()))), obj_ut.prev_line());
        assert_eq!(Some(FilteredLine::ContextLine((1, "one".to_owned()))), obj_ut.prev_line());
        assert_eq!(None, obj_ut.prev_line());
    }

} /* test */


