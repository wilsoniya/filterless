use crate::lines::{
    FilterPredicate,
    FilteredLine,
    IterDirection,
    NumberedLine,
    OptionalFilterPredicate,
};

use super::LineBuffer;

pub type ContextLineNumber = usize;

pub struct ContextBuffer<I: Iterator<Item = String>> {
    lines: LineBuffer<I>,
    context_lines: Vec<FilteredLine>,
    predicate: OptionalFilterPredicate,
    // 1-indexed number of last match; 0 indicates no matches yet found
    last_match_line_num: usize,
}

impl<I: Iterator<Item = String>> ContextBuffer<I> {
    /// Constructs a new `ContextBuffer`.
    ///
    /// ## Params
    /// * `lines` - underlying line buffer
    pub fn new(lines: LineBuffer<I>) -> Self {
        ContextBuffer {
            lines,
            context_lines: Vec::new(),
            predicate: OptionalFilterPredicate::new(None),
            last_match_line_num: 0,
        }
    }

    pub fn set_predicate(&mut self, predicate: OptionalFilterPredicate) {
        self.predicate = predicate;
        self.context_lines = Vec::new();
        self.last_match_line_num = 0;
    }

    /// Gets a single given contextualized line.
    ///
    /// ## Params
    /// * `ctx_line_num` - the context-relative 1-indexed line number to get.
    pub fn get(&mut self, ctx_line_num: ContextLineNumber) -> Option<FilteredLine> {
        let idx = if ctx_line_num > 0 { ctx_line_num - 1 } else { 0 };
        loop {
            let maybe_filtered_line = self.context_lines
                .get(idx)
                .map(|filtered_line| filtered_line.to_owned());

            if maybe_filtered_line.is_some() {
                return maybe_filtered_line
            } else {
                let more_context_was_found = self.extend().is_some();
                if more_context_was_found {
                    continue
                } else {
                    break
                }
            }
        }

        None
    }

    /// Gets an iterator over this buffer.
    ///
    /// # Params
    /// * `start_line_num` - the context-relavie 1-indexed line number of the first context line to
    /// return
    /// * `direction` - the direction in which the iterator will progress
    pub fn iter (
        &'_ mut self,
        start_line_num: ContextLineNumber,
        direction: IterDirection,
    ) -> ContextBufferIter<'_, I> {
        let last_iter_line = match direction {
            IterDirection::BACKWARD => start_line_num + 1,
            IterDirection::FORWARD => if start_line_num > 0 { start_line_num - 1 } else { 0 }
        };

        ContextBufferIter {
            buffer: self,
            last_iter_line,
            direction,
        }
    }

    /// Finds the next matching line within the underlying iterator.
    fn next_match(&mut self) -> Option<NumberedLine> {
        let next_search_line = self.last_match_line_num + 1;
        let predicate = &self.predicate;
        self.lines
            .iter(next_search_line, IterDirection::FORWARD)
            .find(|(_, line)| predicate.matches_line(line))
    }

    fn contextualize(&mut self, match_line_num: usize) -> Vec<FilteredLine> {
        let num_context_lines = match (&self.predicate).into() {
            Some(FilterPredicate{context_lines, ..}) => context_lines,
            None => 0,
        };

        let start = if num_context_lines > match_line_num {
            0
        } else {
            match_line_num - num_context_lines
        };
        let end = match_line_num + num_context_lines + 1;

        let predicate = self.predicate.clone();

        (start..end)
            .flat_map(|idx| {
                match idx {
                    x if x < 1 => None,
                    _ => self.lines.get(idx),
                }
            })
            .map(|numbered_line| predicate.match_line(numbered_line))
            .collect()
    }

    fn extend(&mut self) -> Option<NumberedLine> {
        if let Some((match_line_num, match_line)) = self.next_match() {
            self.last_match_line_num = match_line_num;

            let mut context_block = self.contextualize(match_line_num);
            let maybe_last_context_line: Option<NumberedLine> = self.context_lines
                .last()
                .and_then(|filtered_line| filtered_line.into());

            let maybe_first_block_line: Option<NumberedLine> = (&context_block[0]).into();
            let first_block_line_num = maybe_first_block_line
                .map(|(line_num, _)| line_num)
                .expect("First line of context block was somehow a Gap");

            if let Some((last_context_line_num, _)) = maybe_last_context_line {
                // case: at least one context line stored

                // drop lines in the front of the context block if they're already stored
                context_block = context_block
                    .into_iter()
                    .filter(|filtered_line| {
                        let maybe_numbered_line: Option<NumberedLine> = filtered_line.into();
                        let (this_line_num, _) = maybe_numbered_line
                            .expect("unepectedly got a Gap");
                        this_line_num > last_context_line_num
                    })
                    .collect::<Vec<_>>();

                if context_block.is_empty() {
                    return None
                }

                // TODO hopefully this can be improved
                let maybe_first_block_line: Option<NumberedLine> = (&context_block[0]).into();
                let first_block_line_num = maybe_first_block_line
                    .map(|(line_num, _)| line_num)
                    .expect("First line of context block was somehow a Gap");

                if first_block_line_num != last_context_line_num + 1 {
                    // case: first context block line does not directly follow the last stored
                    // context line
                    self.context_lines.push(FilteredLine::Gap);
                }
            } else {
                // case: zero context lines exist
                if first_block_line_num != 1 {
                    self.context_lines.push(FilteredLine::Gap);
                }
            }

            self.context_lines.extend(context_block);
            Some((match_line_num, match_line))
        } else {
            None
        }
    }
}

pub struct ContextBufferIter<'a, I: Iterator<Item = String>> {
    buffer: &'a mut ContextBuffer<I>,
    last_iter_line: usize,
    direction: IterDirection,
}

impl<'a, I: Iterator<Item = String>> Iterator for ContextBufferIter<'_, I> {
    type Item = FilteredLine;

    fn next(&mut self) -> Option<Self::Item> {
        let maybe_this_line = match self.direction {
            IterDirection::FORWARD => Some(self.last_iter_line + 1),
            IterDirection::BACKWARD => if self.last_iter_line > 1 {
                Some(self.last_iter_line - 1)
            } else {
                None
            },
        };

        maybe_this_line.and_then(|this_line| {
            self.buffer.get(this_line)
                .map(|line| {
                    self.last_iter_line = this_line;
                    line
                })
        })
    }
}

#[cfg(test)]
mod test {
    use crate::buffer::line_buffer::LineBuffer;
    use crate::lines::FilterPredicate;
    use crate::lines::FilteredLine;
    use crate::lines::IterDirection;
    use crate::lines::OptionalFilterPredicate;

    #[test]
    fn match_no_lines_test() {
        let lines: Vec<String> = vec![];
        let line_buffer = LineBuffer::new(lines.iter().cloned());
        let mut obj_ut = super::ContextBuffer::new(line_buffer);
        obj_ut.predicate = OptionalFilterPredicate::new(Some(FilterPredicate {
            pattern: regex::Regex::new("TWO").unwrap(),
            context_lines: 0,
        }));

        assert_eq!(None, obj_ut.next_match());

        // No more matches
        assert_eq!(None, obj_ut.extend());
    }

    #[test]
    fn match_no_match_test() {
        let lines = "one two three four twosome".split_whitespace().map(|line| line.to_owned());
        let line_buffer = LineBuffer::new(lines);
        let mut obj_ut = super::ContextBuffer::new(line_buffer);
        obj_ut.predicate = OptionalFilterPredicate::new(Some(FilterPredicate {
            pattern: regex::Regex::new("TWO").unwrap(),
            context_lines: 0,
        }));

        assert_eq!(None, obj_ut.next_match());

        // No more matches
        assert_eq!(None, obj_ut.extend());
    }

    #[test]
    fn test_next_match() {
        let lines = "one two three four twosome".split_whitespace().map(|line| line.to_owned());
        let line_buffer = LineBuffer::new(lines);
        let mut obj_ut = super::ContextBuffer::new(line_buffer);
        obj_ut.predicate = OptionalFilterPredicate::new(Some(FilterPredicate {
            pattern: regex::Regex::new("two").unwrap(),
            context_lines: 0,
        }));

        assert_eq!(obj_ut.next_match(), Some((2, "two".into())));

        obj_ut.last_match_line_num = 2;
        assert_eq!(obj_ut.next_match(), Some((5, "twosome".into())));
    }

    #[test]
    fn test_contextualize() {
        let lines = "one two three four"
            .split_whitespace()
            .map(|line| line.to_owned());
        let line_buffer = LineBuffer::new(lines);

        let mut obj_ut = super::ContextBuffer::new(line_buffer);
        obj_ut.predicate = OptionalFilterPredicate::new(Some(FilterPredicate {
            pattern: regex::Regex::new("two").unwrap(),
            context_lines: 1,
        }));

        // match in middle of lines, with full context available
        let expected = vec![
            FilteredLine::ContextLine((1, "one".into())),
            FilteredLine::MatchLine((2, "two".into())),
            FilteredLine::ContextLine((3, "three".into())),
        ];
        let actual = obj_ut.contextualize(2);
        assert_eq!(expected, actual);

        // match at beginning of lines
        let expected = vec![
            FilteredLine::ContextLine((1, "one".into())),
            FilteredLine::MatchLine((2, "two".into())),
        ];
        let actual = obj_ut.contextualize(1);
        assert_eq!(expected, actual);

        // match at end of lines
        let expected = vec![
            FilteredLine::ContextLine((3, "three".into())),
            FilteredLine::ContextLine((4, "four".into())),
        ];
        let actual = obj_ut.contextualize(4);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_contextualize_overflow_safety() {
        let lines = "one two three four"
            .split_whitespace()
            .map(|line| line.to_owned());
        let line_buffer = LineBuffer::new(lines);

        let mut obj_ut = super::ContextBuffer::new(line_buffer);
        obj_ut.predicate = OptionalFilterPredicate::new(Some(FilterPredicate {
            pattern: regex::Regex::new("two").unwrap(),
            context_lines: 3,
        }));

        // match in middle of lines, with full context available
        let expected = vec![
            FilteredLine::ContextLine((1, "one".into())),
            FilteredLine::MatchLine((2, "two".into())),
            FilteredLine::ContextLine((3, "three".into())),
            FilteredLine::ContextLine((4, "four".into())),
        ];
        let actual = obj_ut.contextualize(2);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_contextualize_subsequent_matches() {
        let lines = "two two three two"
            .split_whitespace()
            .map(|line| line.to_owned());
        let line_buffer = LineBuffer::new(lines);

        let mut obj_ut = super::ContextBuffer::new(line_buffer);
        obj_ut.predicate = OptionalFilterPredicate::new(Some(FilterPredicate {
            pattern: regex::Regex::new("two").unwrap(),
            context_lines: 2,
        }));

        let expected = vec![
            FilteredLine::MatchLine((1, "two".into())),
            FilteredLine::MatchLine((2, "two".into())),
            FilteredLine::ContextLine((3, "three".into())),
        ];
        let actual = obj_ut.contextualize(1);
        assert_eq!(expected, actual);
    }

    #[test]
    fn extend_without_context_test() {
        let lines = "one two three four twosome"
            .split_whitespace()
            .map(|line| line.to_owned());
        let line_buffer = LineBuffer::new(lines);
        let mut obj_ut = super::ContextBuffer::new(line_buffer);
        obj_ut.predicate = OptionalFilterPredicate::new(Some(FilterPredicate {
            pattern: regex::Regex::new("two").unwrap(),
            context_lines: 0,
        }));

        let expected = Some((2, "two".to_owned()));
        assert_eq!(expected, obj_ut.extend());

        let expected_context_lines = vec![
            FilteredLine::Gap,
            FilteredLine::MatchLine((2, "two".to_owned())),
        ];
        assert_eq!(expected_context_lines, obj_ut.context_lines);

        let expected = Some((5, "twosome".to_owned()));
        assert_eq!(expected, obj_ut.extend());

        let expected_context_lines = vec![
            FilteredLine::Gap,
            FilteredLine::MatchLine((2, "two".to_owned())),
            FilteredLine::Gap,
            FilteredLine::MatchLine((5, "twosome".to_owned())),
        ];
        assert_eq!(expected_context_lines, obj_ut.context_lines);

        // No more matches
        assert_eq!(None, obj_ut.extend());
    }

    #[test]
    fn extend_with_context_test() {
        let lines = "one two three four twosome six seven eight twofer ten eleven"
            .split_whitespace()
            .map(|line| line.to_owned());
        let line_buffer = LineBuffer::new(lines);
        let mut obj_ut = super::ContextBuffer::new(line_buffer);
        obj_ut.predicate = OptionalFilterPredicate::new(Some(FilterPredicate {
            pattern: regex::Regex::new("two").unwrap(),
            context_lines: 1,
        }));

        // first match
        let expected = Some((2, "two".to_owned()));
        assert_eq!(expected, obj_ut.extend());

        let expected_context_lines = vec![
            FilteredLine::ContextLine((1, "one".to_owned())),
            FilteredLine::MatchLine((2, "two".to_owned())),
            FilteredLine::ContextLine((3, "three".to_owned())),
        ];
        assert_eq!(expected_context_lines, obj_ut.context_lines);

        // second match
        let expected = Some((5, "twosome".to_owned()));
        assert_eq!(expected, obj_ut.extend());

        let expected_context_lines = vec![
            FilteredLine::ContextLine((1, "one".to_owned())),
            FilteredLine::MatchLine((2, "two".to_owned())),
            FilteredLine::ContextLine((3, "three".to_owned())),
            FilteredLine::ContextLine((4, "four".to_owned())),
            FilteredLine::MatchLine((5, "twosome".to_owned())),
            FilteredLine::ContextLine((6, "six".to_owned())),
        ];
        assert_eq!(expected_context_lines, obj_ut.context_lines);

        // third match
        let expected = Some((9, "twofer".to_owned()));
        assert_eq!(expected, obj_ut.extend());

        let expected_context_lines = vec![
            FilteredLine::ContextLine((1, "one".to_owned())),
            FilteredLine::MatchLine((2, "two".to_owned())),
            FilteredLine::ContextLine((3, "three".to_owned())),
            FilteredLine::ContextLine((4, "four".to_owned())),
            FilteredLine::MatchLine((5, "twosome".to_owned())),
            FilteredLine::ContextLine((6, "six".to_owned())),
            FilteredLine::Gap,
            FilteredLine::ContextLine((8, "eight".to_owned())),
            FilteredLine::MatchLine((9, "twofer".to_owned())),
            FilteredLine::ContextLine((10, "ten".to_owned())),
        ];
        assert_eq!(expected_context_lines, obj_ut.context_lines);

        // No more matches
        assert_eq!(None, obj_ut.extend());
    }

    #[test]
    fn extend_with_overlapping_context_test() {
        let lines = "two two two two two two two two two"
            .split_whitespace()
            .map(|line| line.to_owned());
        let line_buffer = LineBuffer::new(lines);
        let mut obj_ut = super::ContextBuffer::new(line_buffer);
        obj_ut.predicate = OptionalFilterPredicate::new(Some(FilterPredicate {
            pattern: regex::Regex::new("two").unwrap(),
            context_lines: 1,
        }));

        obj_ut.extend();
        let expected_context_lines = vec![
            FilteredLine::MatchLine((1, "two".to_owned())),
            FilteredLine::MatchLine((2, "two".to_owned())),
        ];
        assert_eq!(expected_context_lines, obj_ut.context_lines);
        assert_eq!(1, obj_ut.last_match_line_num);

        obj_ut.extend();
        let expected_context_lines = vec![
            FilteredLine::MatchLine((1, "two".to_owned())),
            FilteredLine::MatchLine((2, "two".to_owned())),
            FilteredLine::MatchLine((3, "two".to_owned())),
        ];
        assert_eq!(expected_context_lines, obj_ut.context_lines);
    }

    #[test]
    fn get_no_context_test() {
        let lines = "one two three four twosome six seven eight twofer ten eleven"
            .split_whitespace()
            .map(|line| line.to_owned());
        let line_buffer = LineBuffer::new(lines);
        let mut obj_ut = super::ContextBuffer::new(line_buffer);
        obj_ut.predicate = OptionalFilterPredicate::new(Some(FilterPredicate {
            pattern: regex::Regex::new("two").unwrap(),
            context_lines: 0,
        }));

        assert_eq!(Some(FilteredLine::Gap), obj_ut.get(1));
        assert_eq!(Some(FilteredLine::MatchLine((2, "two".to_owned()))), obj_ut.get(2));
        assert_eq!(Some(FilteredLine::Gap), obj_ut.get(3));
        assert_eq!(Some(FilteredLine::MatchLine((5, "twosome".to_owned()))), obj_ut.get(4));
        assert_eq!(Some(FilteredLine::Gap), obj_ut.get(5));
        assert_eq!(Some(FilteredLine::MatchLine((9, "twofer".to_owned()))), obj_ut.get(6));
        assert_eq!(None, obj_ut.get(7));
        assert_eq!(None, obj_ut.get(8));
    }

    #[test]
    fn get_last_first_test() {
        let lines = "one two three four twosome six seven eight twofer ten eleven"
            .split_whitespace()
            .map(|line| line.to_owned());
        let line_buffer = LineBuffer::new(lines);
        let mut obj_ut = super::ContextBuffer::new(line_buffer);
        obj_ut.predicate = OptionalFilterPredicate::new(Some(FilterPredicate {
            pattern: regex::Regex::new("two").unwrap(),
            context_lines: 0,
        }));

        assert_eq!(None, obj_ut.get(8));
        assert_eq!(None, obj_ut.get(7));
        assert_eq!(Some(FilteredLine::MatchLine((9, "twofer".to_owned()))), obj_ut.get(6));
        assert_eq!(Some(FilteredLine::Gap), obj_ut.get(5));
    }

    #[test]
    fn get_with_context() {
        let lines = "one two three four twosome six seven eight twofer ten eleven"
            .split_whitespace()
            .map(|line| line.to_owned());
        let line_buffer = LineBuffer::new(lines);
        let mut obj_ut = super::ContextBuffer::new(line_buffer);
        obj_ut.predicate = OptionalFilterPredicate::new(Some(FilterPredicate {
            pattern: regex::Regex::new("two").unwrap(),
            context_lines: 1,
        }));

        assert_eq!(Some(FilteredLine::ContextLine((1, "one".to_owned()))), obj_ut.get(1));
        assert_eq!(Some(FilteredLine::MatchLine((2, "two".to_owned()))), obj_ut.get(2));
        assert_eq!(Some(FilteredLine::ContextLine((3, "three".to_owned()))), obj_ut.get(3));
        assert_eq!(Some(FilteredLine::ContextLine((4, "four".to_owned()))), obj_ut.get(4));
        assert_eq!(Some(FilteredLine::MatchLine((5, "twosome".to_owned()))), obj_ut.get(5));
        assert_eq!(Some(FilteredLine::ContextLine((6, "six".to_owned()))), obj_ut.get(6));
        assert_eq!(Some(FilteredLine::Gap), obj_ut.get(7));
        assert_eq!(Some(FilteredLine::ContextLine((8, "eight".to_owned()))), obj_ut.get(8));
        assert_eq!(Some(FilteredLine::MatchLine((9, "twofer".to_owned()))), obj_ut.get(9));
        assert_eq!(Some(FilteredLine::ContextLine((10, "ten".to_owned()))), obj_ut.get(10));
        assert_eq!(None, obj_ut.get(11));
        assert_eq!(None, obj_ut.get(12));
        assert_eq!(None, obj_ut.get(13));
    }

    #[test]
    fn iter_basic_test() {
        let lines = "one two three four twosome six seven eight twofer ten eleven"
            .split_whitespace()
            .map(|line| line.to_owned());
        let line_buffer = LineBuffer::new(lines);
        let mut buf = super::ContextBuffer::new(line_buffer);
        buf.predicate = OptionalFilterPredicate::new(Some(FilterPredicate {
            pattern: regex::Regex::new("two").unwrap(),
            context_lines: 0,
        }));

        let mut obj_ut = buf.iter(1, IterDirection::FORWARD);
        assert_eq!(Some(FilteredLine::Gap), obj_ut.next());
        assert_eq!(Some(FilteredLine::MatchLine((2, "two".to_owned()))), obj_ut.next());
        assert_eq!(Some(FilteredLine::Gap), obj_ut.next());
        assert_eq!(Some(FilteredLine::MatchLine((5, "twosome".to_owned()))), obj_ut.next());
        assert_eq!(Some(FilteredLine::Gap), obj_ut.next());
        assert_eq!(Some(FilteredLine::MatchLine((9, "twofer".to_owned()))), obj_ut.next());
        assert_eq!(None, obj_ut.next());
        assert_eq!(None, obj_ut.next());

        let mut obj_ut = buf.iter(3, IterDirection::FORWARD);
        assert_eq!(Some(FilteredLine::Gap), obj_ut.next());
        assert_eq!(Some(FilteredLine::MatchLine((5, "twosome".to_owned()))), obj_ut.next());
        assert_eq!(Some(FilteredLine::Gap), obj_ut.next());
        assert_eq!(Some(FilteredLine::MatchLine((9, "twofer".to_owned()))), obj_ut.next());
        assert_eq!(None, obj_ut.next());
        assert_eq!(None, obj_ut.next());
    }

    #[test]
    fn iter_backwards_test() {
        let lines = "one two three four twosome six seven eight twofer ten eleven"
            .split_whitespace()
            .map(|line| line.to_owned());
        let line_buffer = LineBuffer::new(lines);
        let mut buf = super::ContextBuffer::new(line_buffer);
        buf.predicate = OptionalFilterPredicate::new(Some(FilterPredicate {
            pattern: regex::Regex::new("two").unwrap(),
            context_lines: 0,
        }));

        let mut obj_ut = buf.iter(6, IterDirection::BACKWARD);
        assert_eq!(Some(FilteredLine::MatchLine((9, "twofer".to_owned()))), obj_ut.next());
        assert_eq!(Some(FilteredLine::Gap), obj_ut.next());
        assert_eq!(Some(FilteredLine::MatchLine((5, "twosome".to_owned()))), obj_ut.next());
        assert_eq!(Some(FilteredLine::Gap), obj_ut.next());
        assert_eq!(Some(FilteredLine::MatchLine((2, "two".to_owned()))), obj_ut.next());
        assert_eq!(Some(FilteredLine::Gap), obj_ut.next());
        assert_eq!(None, obj_ut.next());

        let mut obj_ut = buf.iter(3, IterDirection::BACKWARD);
        assert_eq!(Some(FilteredLine::Gap), obj_ut.next());
        assert_eq!(Some(FilteredLine::MatchLine((2, "two".to_owned()))), obj_ut.next());
        assert_eq!(Some(FilteredLine::Gap), obj_ut.next());
        assert_eq!(None, obj_ut.next());
    }

    #[test]
    fn iter_backwards_beyond_endtest() {
        let lines = "one two three four twosome six seven eight twofer ten eleven"
            .split_whitespace()
            .map(|line| line.to_owned());
        let line_buffer = LineBuffer::new(lines);
        let mut buf = super::ContextBuffer::new(line_buffer);
        buf.predicate = OptionalFilterPredicate::new(Some(FilterPredicate {
            pattern: regex::Regex::new("two").unwrap(),
            context_lines: 0,
        }));

        let mut obj_ut = buf.iter(7, IterDirection::BACKWARD);
        assert_eq!(None, obj_ut.next());
        assert_eq!(None, obj_ut.next());
        assert_eq!(None, obj_ut.next());
        // ... this will never end
    }
}
