use std::cmp::{max, min};

use ncurses;

use lines::{
    FilteredLine,
    LineFragment,
    OptionalFilterPredicate,
};

use crate::buffer::WindowBuffer;

pub struct Pager<T: Iterator<Item=String>> {
    window: ncurses::WINDOW,
    height: usize,
    num_digits: usize,
    window_buffer: WindowBuffer<T>,
    predicate: OptionalFilterPredicate,
    /// 0-indexed of the bottom-most on-screen viewable line number [0, height -1]
    bottom_cursor_pos: usize,
}

impl<T: Iterator<Item=String>> Pager<T> {
    pub fn new(window: ncurses::WINDOW, iter: T) -> Pager<T> {
        ncurses::start_color();
        ncurses::init_pair(1, ncurses::constants::COLOR_BLACK,
                           ncurses::constants::COLOR_YELLOW);
        ncurses::init_pair(2, ncurses::constants::COLOR_GREEN,
                           ncurses::constants::COLOR_BLACK);
        ncurses::init_pair(3, ncurses::constants::COLOR_RED,
                           ncurses::constants::COLOR_BLACK);

        let mut height = 0;
        let mut width = 0;
        ncurses::getmaxyx(window, &mut height, &mut width);
        ncurses::wclear(window);
        ncurses::scrollok(window, true);
        ncurses::idlok(window, true);

        let predicate = OptionalFilterPredicate::new(None);
        let window_buffer = WindowBuffer::new(iter, height as usize);

        Pager {
            window,
            height: height as usize,
            num_digits: 1,
            predicate,
            window_buffer,
            bottom_cursor_pos: 0,
        }
    }

    pub fn next_line(&mut self) {
        let lines = self.window_buffer
            .next_line()
            .into_iter()
            .collect();
        self.blit_lines(lines, Direction::Down);
    }

    pub fn prev_line(&mut self) {
        let lines = self.window_buffer
            .prev_line()
            .into_iter()
            .collect();
        self.blit_lines(lines, Direction::Up);
    }

    pub fn next_page(&mut self){
        let lines = self.window_buffer.next_page();
        self.blit_lines(lines, Direction::Down);
    }

    pub fn prev_page(&mut self) {
        let lines = self.window_buffer.prev_page();
        self.blit_lines(lines, Direction::Up);
    }

    fn blit_lines(&mut self, lines: Vec<FilteredLine>, direction: Direction) {
        if lines.is_empty() {
            return
        }

        // Only scroll if blitting less than a full page
        let scroll_offset = match (&direction, lines.len()) {
            // must create blank space on top when blitting above
            (Direction::Up, x) if x < self.height => -(lines.len() as i32),
            // only need one blank line because lines blitted below have newlines
            (Direction::Down, x) if x < self.height => 1,
            // full pages don't need scrolling
            _ => 0,
        };
        let cursor_pos = match direction {
            Direction::Up => 0,
            Direction::Down => self.bottom_cursor_pos as i32,
        };

        ncurses::wscrl(self.window, scroll_offset);
        ncurses::wmove(self.window, cursor_pos, 0);

        lines
            .iter()
            .enumerate()
            .for_each(|(idx, filtered_line)| {
                self.print_line(&filtered_line);

                if idx < lines.len() -1 {
                    ncurses::waddstr(self.window, "\n");
                }
            });

        ncurses::wrefresh(self.window);

        self.increment_bottom_cursor_pos(match direction {
            Direction::Up => 0,
            Direction::Down => lines.len(),
        });
    }

    pub fn filter(&mut self, predicate: OptionalFilterPredicate) {
        self.window_buffer.set_predicate(predicate.clone());
        self.predicate = predicate;
        self.reset_bottom_cursor_pos();
        ncurses::wclear(self.window);
        self.next_page();
    }

    fn increment_bottom_cursor_pos(&mut self, num_lines: usize) {
        self.bottom_cursor_pos = min(
            self.height - 1,
            self.bottom_cursor_pos + num_lines,
        );
    }

    fn reset_bottom_cursor_pos(&mut self) {
        self.bottom_cursor_pos = 0;
    }

    fn print_line_num(&mut self, line_num: usize) {
        self.num_digits = max(3, max(self.num_digits, (line_num as f32)
            .log10()
            .floor() as usize + 1));
        ncurses::wattron(self.window, ncurses::COLOR_PAIR(2));
        ncurses::waddstr(self.window, &format!("{:>1$} ", line_num, self.num_digits));
        ncurses::wattroff(self.window, ncurses::COLOR_PAIR(2));
    }

    fn print_line(&mut self, filtered_line: &FilteredLine) {
        match *filtered_line {
            FilteredLine::Gap => {
                ncurses::waddstr(self.window, "-----");
            },
            FilteredLine::ContextLine((line_num, ref line)) => {
                self.print_line_num(line_num);
                ncurses::waddstr(self.window, line);

            },
            FilteredLine::MatchLine((line_num, ref line)) => {
                self.print_line_num(line_num);

                for frag in self.predicate.get_fragments(line) {
                    match frag {
                        LineFragment::Match(text) => {
                            ncurses::wattron(self.window, ncurses::COLOR_PAIR(1));
                            ncurses::waddstr(self.window, text);
                            ncurses::wattroff(self.window, ncurses::COLOR_PAIR(1));
                        },
                        LineFragment::NoMatch(text) => {
                            ncurses::waddstr(self.window, text);
                        },
                    }
                }
            },
            FilteredLine::UnfilteredLine((line_num, ref line)) => {
                self.print_line_num(line_num);
                ncurses::waddstr(self.window, line);
            },
        }
    }
}

enum Direction {
    Up,
    Down,
}
